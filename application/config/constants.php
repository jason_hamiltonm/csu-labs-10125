<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


define('V', '1.0.0'); //setup a version

// define 2 local environments and a stage server
$extension = '';
if(ENVIRONMENT=='localhost') $extension = '/lab0';

define('IMG', ENVIRONMENT=='localhost' || ENVIRONMENT == 'localhost2'? $extension.'/assets/img/' : '/~jasonhm/10125/lab0/assets/img/');
define('JS', ENVIRONMENT=='localhost' || ENVIRONMENT == 'localhost2'? $extension.'/assets/js/' : '/~jasonhm/10125/lab0/assets/js/');
define('CSS', ENVIRONMENT=='localhost' || ENVIRONMENT == 'localhost2'? $extension.'/assets/css/' : '/~jasonhm/10125/lab0/assets/css/');
define('URL', ENVIRONMENT=='localhost' || ENVIRONMENT == 'localhost2'? $extension.'/' : '/~jasonhm/10125/lab0/index.php?/');
define('VENDOR', ENVIRONMENT=='localhost' || ENVIRONMENT == 'localhost2'? $extension.'/vendor/' : '/~jasonhm/10125/lab0/vendor/');
define('POSTURL', ENVIRONMENT=='localhost' || ENVIRONMENT == 'localhost2'? $extension.'/' : 'index.php?');


/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */
<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Auth Notes - Simple Authentication
 *
 * This is meant to mimic the behaviour found on ion_auth or tank_auth libraries
 *
 *
 */
class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('auth_library');

        $this->data = '';

    }

    public function index()
    {
        $this->login();
    }

    public function login()
    {

        if($this->input->post()){

            $this->auth_library->login($this->input->post());

            if($this->session->userdata('user_id') > 0)
                $this->load->view('auth/dashboard', $this->data);
            else {
                $this->data['err'] = "<div class=\"alert alert-danger\">Username or Password Incorrect</div>";
                $this->load->view('auth/login', $this->data);
            }
        }else {

            $this->load->view('auth/login', $this->data);

        }
    }

    public function register()
    {

        if($this->input->post()){

            $this->auth_library->register($this->input->post());

            echo "<div class=\"alert alert-success\">Thanks for Registering</div> <br />";

        }

        $this->load->view('auth/register', $this->data);
    }




}

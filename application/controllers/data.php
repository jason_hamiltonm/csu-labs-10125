<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Data extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        // load the model now
        $this->load->model('Rooms_model', 'rooms'); // use "rooms" to call db model methods

        // get the initial data for every method now in the constructor
        $this->refresh();

    }

    // Database Model Examples and Setup (CRUD)


    /**
     *  get global data for every method here
     */
    public function refresh()
    {
        $this->data['rooms'] = $this->rooms->get();
        $this->data['how_many'] = $this->rooms->how_many();
    }

    /**
     * get list of rooms in database
     */
    public function index()
    {

        $this->load->view('data', $this->data);

    }

    /**
     * edit facility
     *
     * @param $id - room id
     */
    public function edit($id)
    {

        // or use $this->uri->segment(3) for example where 3 is the URL segment place... test.com/data/edit/45454 .. thus  $id = 45454
        // also, you can add additional parameters such as edit($id,$name=false,$value='') and the url will look like test.com/data/edit/45454/jason/3/
        // data = controller
        // edit = method
        // 45454 = $id
        // jason = $name
        // 3 = $value

        $this->data['room_to_edit'] = $this->rooms->get(array('id' => $id));

        $this->load->view('data', $this->data);

    }

    /**
     *  save an edit
     */
    public function save()
    {
        if ($_POST) {
            //quick way to save or update from a form with form values that equal the database fields

            $return = $this->rooms->save($this->input->post());

            if ($return == false)
                die('error updating the database');
        }

        $this->refresh();
        $this->index();


    }

    /**
     * delete a room
     *
     * @param $id - room id
     */
    public function delete($id)
    {

        $return = $this->rooms->delete($id);

        if ($return == false)
            die('error deleting the record');

        $this->refresh();
        $this->index();


    }

    public function delete_ajax()
    {

        // make sure this is an ajax call
        if($this->input->is_ajax_request()) {

            $room_id = $this->input->post('room_id');

            echo "Something went right!"; // this passes some sort of message

            // uncomment below to make this work
            //$return = $this->rooms->delete($room_id);

            //if ($return == false)
                //die('error deleting the record');

        }

    }

    public function browse()
    {

        // this is setup for my localhost environment

        if(ENVIRONMENT=='development')
            $upload_path = "https://csu.mohawkcollege.ca/~jasonhm/10125/lab0/uploads/";
        else
            $upload_path = "http://localhost/uploads/";

        ?>


        <a href=# onClick = " window.opener.CKEDITOR.tools.callFunction( 1 ,'<?=$upload_path?>logo.png') ; self.close();"><img src="<?=$upload_path?>logo.png" /></a>
        <?



    }

    public function upload()
    {

        // this is where you process the upload from ckeditor

    }


}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Template_library');
        $this->load->helper('form');
        $this->load->helper('url'); // or use array for both form and url


    }


	public function index()
	{

        $data = array();
        $data['title'] = "Lab 0";

        $this->template_library->show('home', $data);


	}

    //Form Validation Class
    public function post() {

        $this->load->library('form_validation');

        $data['username'] = $this->input->post('username',TRUE);
        $data['firstname'] = $this->input->post('firstname',TRUE);
        $data['lastname'] = $this->input->post('lastname',TRUE);
        $data['age'] = $this->input->post('age',TRUE);

        $data['map'] = $this->TPL;
        
        $this->form_validation->set_error_delimiters("<span style='color:red'>", '</span>');
        $this->form_validation->set_rules('username', 'User name', 'callback_username_check');
        $this->form_validation->set_rules('firstname', 'First name', 'trim|required|min_length[3]|max_length[25]');
        $this->form_validation->set_rules('lastname', 'Last name', 'trim|required|xss_clean ');
        $this->form_validation->set_rules('age', 'Age', 'trim|required|integer');
        $this->form_validation->set_rules('program', 'Programs', 'required');

        if ($this->form_validation->run() == FALSE):
            $data['msg'] = 'Errors still in form';
            $data['title'] = "Lab 0 :: Contact Us Demo";
            $this->template_library->show('contact', $data);
            return;
        endif;

        $this->session->set_flashdata('message', 'Thank you for your interest.');
        redirect('/home/contact');


    }

    //Call back function for form validation
    function username_check($str)
    {
        $str = trim($str);
        if (empty($str) == true):
            $this->form_validation->set_message('username_check', '%s is required field.');
            return FALSE;
        endif;
        if (preg_match('/^[a-zA-z09]{3,9}$/',$str) == false):
            $this->form_validation->set_message('username_check', 'Only character/numbers allowed 3-9 chars');
            return FALSE;
        endif;
        return TRUE;  //all good
    }

    function mailMe() {

        $this->load->library('email');

        $this->email->from('jasonhm@csu.mohawkcollege.ca', 'Big Guy');
        $this->email->to('jasonhm@csu.mohawkcollege.ca');

        $this->email->subject('Email From SleepyMe Contact From');
        $this->email->message('IP Address of Sender: '. $this->input->ip_address());

        if (   $this->email->send() == false ):
            $data['msg'] = "Email Failed.... <br>";
        else:
            $data['msg'] = "Email sent. Check it with Pine ";

        endif;
        $data['msg'] .= "<br>". $this->email->print_debugger();

        $data['title'] = "Lab 0 :: Mail Me Demo";
        $this->template_library->show('default', $data);


    }

    public function contact()
    {

        $data = array();
        $data['title'] = "Lab 0 :: Contact Us Demo";

        $data['username'] = '';
        $data['firstname'] = '';
        $data['lastname'] = '';


        //$data['map'] = $this->TPL;

        $this->template_library->show('contact', $data);

    }


    public function mapit()
    {
        $this->load->view('mapit');
    }

    public function printLibrary()
    {
        highlight_file('application/libraries/template_library.php');

        exit;
    }

    public function printAsset()
    {
        highlight_file('application/config/asset_url.php');

        exit;
    }

    public function printAutoload()
    {
        highlight_file('application/config/autoload.php');

        exit;
    }

    public function printConfig()
    {
        highlight_file('application/config/config.php');

        exit;
    }

    public function printConstants()
    {
        highlight_file('application/config/constants.php');

        exit;
    }
    public function printRoutes()
    {
        highlight_file('application/config/routes.php');

        exit;
    }

    public function printController()
    {
        highlight_file('application/controllers/home.php');

        exit;
    }

    public function printHelper()
    {
        highlight_file('application/hellpers/asset_helper.php');

        exit;
    }

    public function printHtaccess()
    {
        highlight_file('.htaccess');

        exit;
    }

    public function printView()
    {
        echo "<h2>/views/home.php</h2>";

        highlight_file('application/views/home.php');

        echo "<h2>/views/header.php</h2>";

        highlight_file('application/views/header.php');

        echo "<h2>/views/footer.php</h2>";

        highlight_file('application/views/footer.php');

        echo "<h2>/views/contact.php</h2>";

        highlight_file('application/views/contact.php');

        exit;
    }






}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
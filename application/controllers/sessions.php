<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Sessions Notes
 *
 * Autoloaded "session" library to use CodeIgniter's session (NOT PHP NATIVE SESSIONS)
 * Note: remember to set encryption key in config/config.php
 *
 */

class Sessions extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Template_library');
        $this->load->helper('form');
        $this->load->helper('url'); // or use array for both form and url

    }

    public function reset() {

        $this->session->unset_userdata('message'); // remove message session
        $this->session->set_flashdata('info','We Just RESET message!'); // create a flash session variable that will have a life of on page refresh

        redirect(base_url().'index.php?/sessions','refresh'); // redirect back to the page we are on with a refresh

    }

	public function index()
	{

        // do not echo anything before set_userdata

        if ($this->session->userdata('message')) { // check if session for message exists
            $increment = $this->session->userdata('message') + 1; // exists so add 1
            $this->session->set_userdata('message',$increment); // store the new value
        }else{
            $this->session->set_userdata('message',1); // lets create the session variable for the first time
        }

        // this is where we can display page info or view

        echo "<h1>How to: Click the REFRESH button on your browser to increment page count.</h1>";

        // check for flash data message, if found echo to user
        if($this->session->flashdata('info'))
            echo "<h3>".$this->session->flashdata('info') . "</h3>";

        echo "We are at: " . $this->session->userdata('message');

        //  reset link
        echo "<a href=\"".URL."sessions/reset\">Reset</a>";
	}


}

/* End of file sessions.php */
/* Location: ./application/controllers/sessions.php */
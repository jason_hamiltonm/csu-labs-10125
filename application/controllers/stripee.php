<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Stripe Notes
 *
 * Refer to : https://stripe.com/docs/checkout/guides/php
 *
 *
 */

class Stripee extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url'); // or use array for both form and url

        // setup the stripe credentials here
        $this->stripe = array(
            "secret_key"      => "sk_test_PQuOLMsE82XttXQAFdREGmur",
            "publishable_key" => "pk_test_lQTVJgx88qykSAnwPFpVYy1Z"
        );

    }


    public function charge()
    {

        // lets charge the credit card & create the customer etc....

        if(ENVIRONMENT=='localhost2')
            require_once('vendor/stripe/lib/Stripe.php');
        else
            require_once('/home/faculty/jasonhm/public_html/10125/lab0/vendor/stripe/lib/Stripe.php');

        Stripe::setApiKey($this->stripe['secret_key']);

        // setup the credit card array for stripe
        $myCard['number'] = $this->input->post('number');
        $myCard['exp_month'] = $this->input->post('exp_month');
        $myCard['exp_year'] = $this->input->post('exp_year');

        $customer = Stripe_Customer::create(array(
            'email' => $this->input->post('email'),
            'card'  => $myCard
        ));

        // CHARGE IT!
        $charge = Stripe_Charge::create(array(
            'customer' => $customer->id,
            'amount'   => $this->input->post('amount'), //in cents
            'currency' => 'cad'
        ));

        // check the charge variable in json format for any errors here (todo)

        echo '<h1>Successfully charged $'.($this->input->post('amount') / 100).'!</h1>';
    }

	public function index()
	{

        // create the stripe form
        $this->load->view('stripe',$this->stripe);
	}


}

/* End of file stripe.php */
/* Location: ./application/controllers/stripe.php */
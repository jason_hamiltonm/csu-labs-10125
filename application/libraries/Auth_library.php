<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 *  This template library can be used to call auth methods
 *
 *    Usage: $this->template->register($args);
 *    Note: make sure to include in autoload.php or load before calling
 *
 *
 */
class Auth_library
{
    function register($args = NULL)
    {
        $CI =& get_instance();

        $CI->load->model('auth_model');

        $CI->auth_model->insert($args);

    }

    function login($args = NULL)
    {
        $CI =& get_instance();

        $CI->load->model('auth_model');

        // no blanks
        if($args['email'] == '' || $args['password']=='')
            $result = FALSE;
        else
            $result = $CI->auth_model->get($args);

        if($result) {
            $CI->session->set_userdata('user_id',$result['id']);
            $CI->session->set_userdata('role',$result['role']);
            $CI->session->set_userdata('first_name',$result['first_name']);

        }else{
            $CI->session->set_userdata('user_id',0); // make sure we are not logged in
        }

    }

    // we can create other library functions to be accessed from any controller, model or view

    function is_admin() {


    }

    function is_logged_in() {

    }

    function logout() {
        // can just do this from controller unless you need a bunch of processes or tasks to be accomplished after logout
    }

    /// etc....
}
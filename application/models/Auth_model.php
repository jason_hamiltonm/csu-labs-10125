<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


class Auth_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();

        $this->_table = "users";
    }


    function get($params = array())
    {

        // must have params
        if(empty($params))
            return FALSE;

        $id = isset($params['id']) ? $params['id'] : false;

        $email = isset($params['email']) ? $params['email'] : false;
        $password = isset($params['password']) ? $params['password'] : false;

        $id ? $this->db->where('id', $id) : '';

        $email ? $this->db->where('email', $email) : '';

        $password ? $this->db->where('password', md5(sha1($password))) : '';

        $query = $this->db->get($this->_table);

        $result = $query->result_array();

        if(!isset($result[0]))
            return FALSE;

        return $id || $email || $password ? $result[0] : $result;  //array returned

    }


    function save($post_data)
    {

        if (isset($post_data['id'])) {

            unset($post_data['submit']); // don't need this

            $this->db->where('id', $post_data['id']);

            unset($post_data['id']); // leaving this will cause a db error

            return $this->db->update($this->_table, $post_data);

        }else{

            return FALSE;

        }

    }


    function insert($post_data)
    {

        unset($post_data['submit']); // don't need this

        // add stamp
        $post_data['stamp'] = date('Y-m-d H:i:s');

        // if password, encrypt it with sha1
        if(isset($post_data['password'])) {

            $post_data['password'] = md5(sha1($post_data['password']));

        }

        return $this->db->insert($this->_table, $post_data);

    }


    function delete($id)
    {

        return $this->db->delete($this->_table, array('id' => $id));

    }


}


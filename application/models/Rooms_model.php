<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class Rooms_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $this->_table = "rooms";
    }


    // keep functions as simple as possible

    function get($params = array()) {

        $id = isset($params['id']) ? $params['id'] : false;

        $id ? $this->db->where('id',$id) : '';

        $query = $this->db->get($this->_table);

        return $query->result_array();  //array returned

    }


    // this is a different way to pull the number of records
    function how_many() {
        $query = $this->get();

        return count($query);
    }

    function save($post_data) {

        unset($post_data['submit']); // don't need this

        $this->db->where('id',$post_data['id']);

        unset($post_data['id']); // leaving this will cause a db error

        return $this->db->update($this->_table,$post_data);

    }

    function delete($id) {

        return $this->db->delete($this->_table,array('id'=>$id));

    }






}
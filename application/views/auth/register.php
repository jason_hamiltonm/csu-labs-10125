<!DOCTYPE html>
<html lang="en">
<head>

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script src="<?=JS?>bootstrap.js"></script>
    <script src="<?=JS?>cycle2.js"></script>

    <!-- Le styles -->
    <link href="<?=CSS?>bootstrap.css" rel="stylesheet">

</head>
<body>

<form role="form" method="post" action="<?=base_url().'index.php?/auth/register'?>">
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
    </div>

    <div class="form-group">
        <label for="first">First Name</label>
        <input type="text" name="first_name" class="form-control" id="first" placeholder="Enter first name">
    </div>

    <div class="form-group">
        <label for="last">Last Name</label>
        <input type="text" name="last_name" class="form-control" id="last" placeholder="Enter last name">
    </div>

    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="">
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
</form>


</body>
</html>
<body>
<div class="container">

    <div class="masthead">
        <h3 class="muted">Demo Project</h3>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <ul class="nav">
                        <li><a href="<?=URL?>">Home</a></li>
                        <li><a href="<?=URL?>data">Data</a></li>
                        <li><a href="<?=URL?>sessions">Sessions</a></li>
                        <li><a href="<?=URL?>stripee">Stripe</a></li>

                        <li class="active"><a href="<?=URL?>home/contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div><!-- /.navbar -->
    </div>



    <hr>

   <div>

       <?
       // setup form - you can also have this sitting in the controller or define variable
       $programOptions = array(""    =>    "Please Select One",
           "technology"     =>     "Technology",
           "business"        =>     "Business",
           "environment"    =>    "Environment");?>


       <style>
           #map-canvas {
               height: 300px;
               width: 300px;
               margin: 0px;
               padding: 0px;
               float:right;
           }
       </style>
       <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
       <script>
           var map;
           function initialize() {
               var mapOptions = {
                   zoom: 8,
                   center: new google.maps.LatLng(-34.397, 150.644)
               };
               map = new google.maps.Map(document.getElementById('map-canvas'),
                   mapOptions);
           }

           google.maps.event.addDomListener(window, 'load', initialize);

       </script>


       <div id="map-canvas"></div>





       <!--Form created using the Form Helper Functions in conjunction with Form Validation Class-->
       <div style="float:left;">
           <h3><?= isset($msg) ? $msg : ''; ?> <?= $this->session->flashdata('message')!='' ? $this->session->flashdata('message') : ''; ?></h3>

           <?= form_open(POSTURL.'/home/post', array('role' => 'form')); ?>

           <?php //echo validation_errors(); ?>


           <div class="form-group">
               <label for="username">User Name:</label>

               <?= form_input(array('name'    => 'username',
                       'value' => set_value('username',$username),
                       'size'    => 50) ); ?>
               <?= form_error('username'); ?>
           </div>

           <div class="form-group">
               <label for="firstname">First Name:</label>

               <?= form_input(array('name'    => 'firstname',
                       'value' => set_value('firstname',$firstname),
                       'size'    => 50) ); ?>
               <?= form_error('firstname'); ?>
           </div>

           <div class="form-group">
               <label for="lastname">Last Name:</label>

               <?= form_input(array('name'    => 'lastname',
                       'value' => set_value('lastname',$firstname),
                       'size'    => 50) ); ?>
               <?= form_error('lastname'); ?>
           </div>

           <div class="form-group">
               <label for="age">Age:</label>

               <?= form_input(array('name'    => 'age',
                       'value' => set_value('age'),
                       'size'    => 10) ); ?>
               <?= form_error('age'); ?>
           </div>

           <div class="form-group">
               <label for="program">Program:</label>
               <?= form_dropdown('program',$programOptions,
                   $this->input->post('program') ,
                   'size="1" id="select"' ) ?>
               <?= form_error('program'); ?>
           </div>


           <?= form_submit('submitForm', 'Send Form' ); ?>
           <?= form_close(); ?>

       </div>




   </div>
    <div style="clear:both;"></div>

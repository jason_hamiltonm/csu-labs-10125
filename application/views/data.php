<html>

<head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="<?=VENDOR?>ckeditor/ckeditor.js"></script>

    <script>

        $(document).ready(function() {

            // This will work because we are listening on the 'document',
            // for a click on an element with an ID of #test-element
            $(document).on("click","#test-element",function(e) {
                e.preventDefault();

                alert("click bound to document listening for #test-element");
            });

            // This will not work because there is no '#test-element'
            $("#test-element").on("click",function(e) {
                e.preventDefault();

                alert("click bound directly to #test-element");
            });

            // delete method converted to ajax
            $(document).on('click', '.delete_me', function (e) {
                e.preventDefault();

                var $room_id = $(this).data('id');

                $.post('<?=URL?>data/delete_ajax', {room_id: $room_id}, function (data) {

                    // show the response on the page
                    $('.screen-message').html(data);
                    $(".room_row_" + $room_id).hide();


                }).fail(function () {

                    // just in case posting your form failed
                    $('.screen-message').html('Something went wrong');

                });

            });


            // Create a dynamic element
            $('body').append('<a href="#" id="test-element">Click mee</a>');


        });

    </script>
</head>

<h2>Rooms</h2>

<div class="screen-message"></div>

<table cellspacing="5" cellpadding="10" id="myTable">

    <tr><td>Room Name</td><td>Room No</td><td>Rate</td><td>Actions</td></tr>
<?php

foreach($rooms as $room) { ?>


    <tr class="room_row_<?=$room['id']?>">
        <td><?=$room['name']?></td>
        <td><?=$room['room_no']?></td>
        <td>$<?=$room['rate']?></td>
        <td><a href="<?=URL?>data/edit/<?=$room['id']?>">Edit</a> | <a class="delete_me" data-id="<?=$room['id']?>" href="#">Delete</a></td>
    </tr>


<?php } ?>
</table>

<p>How many? <?=$how_many?></p>


<?php


if(isset($room_to_edit)) { $room_to_edit = $room_to_edit[0];?>

    <form action="<?=URL?>data/save" method="post">

        <div class="form-group">

            <input type="hidden" name="id" value="<?=$room_to_edit['id']?>" />

            <label for="username">Name:</label>
            <input type="text" name="name"  value="<?=$room_to_edit['name']?>"/>

            <label for="username">Room No:</label>
            <input type="text" name="room_no" value="<?=$room_to_edit['room_no']?>"/>

            <label for="username">Rate:</label>
            <input type="text" name="rate" value="<?=$room_to_edit['rate']?>" />

            <label for="username">Description:</label>
            <textarea name="description" id="desc"><?=$room_to_edit['description']?></textarea>


            <input type="submit" name="submit" value="submit" />
        </div>


        <script>
            CKEDITOR.replace( 'desc', {
                filebrowserBrowseUrl: '<?=URL?>data/browse/',
                filebrowserUploadUrl: '<?=URL?>data/upload/'
            });

        </script>

    </form>


<?php } ?>








</html>


<body>

<div class="container">

    <div class="masthead">
        <h3 class="muted">Demo Project</h3>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <ul class="nav">
                        <li class="active"><a href="<?=URL?>">Home</a></li>
                        <li><a href="<?=URL?>data">Data</a></li>
                        <li><a href="<?=URL?>sessions">Sessions</a></li>
                        <li><a href="<?=URL?>stripee">Stripe</a></li>
                        
                        <li><a href="<?=URL?>home/contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div><!-- /.navbar -->
    </div>

    <div class="cycle-slideshow"
         data-cycle-fx="scrollHorz"
         data-cycle-timeout="0"
         data-cycle-slides="> div"
        >

        <!-- Jumbotron -->

        <div class="jumbotron" data-cycle-hash="home" style="height:350px; width:100%;">
            <h1>Marketing stuff!</h1>
            <p><img src = "<?= assetUrl('record-player.png'); ?>" width="100px"></p>
            <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            <a class="btn btn-large btn-success" href="#login">Get started today</a>
        </div>

        <div class="jumbotron" data-cycle-hash="login" style="height:350px; width:100%;">

                <form class="form-signin">
                    <h2 class="form-signin-heading">Please sign in</h2>
                    <input type="text" class="input-block-level" placeholder="Email address">
                    <input type="password" class="input-block-level" placeholder="Password">
                    <label class="checkbox">
                        <input type="checkbox" value="remember-me"> Remember me
                    </label>
                    <button class="btn btn-large btn-primary" type="submit">Sign in</button>
                </form>

        </div>

    </div>

    <hr>

    <!-- Example row of columns -->
    <div class="row-fluid">
        <div class="span4">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn" href="#">View details &raquo;</a></p>
        </div>
        <div class="span4">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn" href="#">View details &raquo;</a></p>
        </div>
        <div class="span4">
            <h2>Heading</h2>
            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa.</p>
            <p><a class="btn" href="#">View details &raquo;</a></p>
        </div>
    </div>

    <hr>

